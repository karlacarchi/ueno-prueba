define([], function () { 

  function LoanManager() { 
    kony.mvc.Business.Controller.call(this); 
  } 
  inheritsFrom(LoanManager, kony.mvc.Business.Delegator); 
  LoanManager.prototype.initializeBusinessController = function() {}; 

  LoanManager.prototype.execute = function(command) { 
    kony.mvc.Business.Controller.prototype.execute.call(this, command);
  };

  LoanManager.prototype.applyLoan = function(params, presentationSuccessCallback,
                                              presentationErrorCallback){
    var self = this;
    alert ("Input Params . " + JSON.stringify(params));
    var accountList = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("loan");
    accountList.customVerb('applyLoan', params, addLoansCompletionCallback);

    function addLoansCompletionCallback (status, data, error){
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      kony.print("Data from applyLoan... " + JSON.stringify(obj));

      if(obj["status"] === true){
        presentationSuccessCallback(obj["data"]);
      }else{
        presentationErrorCallback(obj["errmsg"]);
      }        
    }
  };



  return LoanManager;

});