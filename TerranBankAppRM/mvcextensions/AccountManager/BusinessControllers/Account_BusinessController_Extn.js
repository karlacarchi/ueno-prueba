define({
  updateAccount : function(params, presentationSuccessCallback, presentationErrorCallback){
	alert("In AccountManager Extn For Update Account");
    var self = this;
    var accountList = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("account");
    accountList.customVerb('updateAccount', params, updateAccountCompletionCallback);
    
    function updateAccountCompletionCallback(status, data, error){
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      kony.print("data from Update Account..." + JSON.stringify(obj));
      
      if(obj["status"] === true){
        presentationSuccessCallback(obj["data"]);
      }else{
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  }
  
});
