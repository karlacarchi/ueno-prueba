//Update the presentation "base file" for transactions
define([], function() {
  function Transaction_PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
  }
  inheritsFrom(Transaction_PresentationController, kony.mvc.Presentation.BasePresenter);
  Transaction_PresentationController.prototype.initializePresentationController = function() {
  };

  //THIS IS JUST TO CALL TO THE METHOD IN BUSINESS CONTROLLER
  Transaction_PresentationController.prototype.getTransactionsAndShow = function() {    
    var transactionManger = applicationManager.getTransactionManager();    	
    transactionManger.getTransactions(this.getTransactionsSC.bind(this),this.getTransactionsEC.bind(this));  
  };
  /*  getTransactions Success Callback  */  
  Transaction_PresentationController.prototype.getTransactionsSC = function(response){    

    var transactionNavObj = applicationManager.getNavigationManager();    	
    transactionNavObj.setCustomInfo("frmTransactions",response);    	
    transactionNavObj.navigateTo("frmTransactions");  
  };  

  /*  getTransactions Error Callback  */  
  Transaction_PresentationController.prototype.getTransactionsEC = function(errMsg){   

    var transactionNavObj = applicationManager.getNavigationManager();    	
    transactionNavObj.setCustomInfo("frmTransactions",errMsg);    
    alert("Data Not Available  ###### "+JSON.stringify(errMsg));    	
    transactionNavObj.navigateTo("frmTransactions");  
  };
  return Transaction_PresentationController;
});