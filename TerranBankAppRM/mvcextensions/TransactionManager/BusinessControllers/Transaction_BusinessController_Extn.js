define({
  getTransactions : function(params,presentationSuccessCallback,presentationErrorCallback){
    alert("In Transaction BC Extension");  
    var self = this;  
    var accountId = params.accountid;  
    var accountList= kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("transaction");  
    var filterParam = {  
      "$filter": "('fromaccountid' eq " + accountId + " ) "  
    };  
    
    accountList.customVerb('getTransactions', filterParam, getTransactionsCompletionCallback);      
    function getTransactionsCompletionCallback(status, data, error) {    
      var srh = applicationManager.getServiceResponseHandler();    
      var obj = srh.manageResponse(status, data, error);    
      kony.print("Data from transactions ... "+JSON.stringify(obj));    
      if (obj.status === true) {     
        presentationSuccessCallback(obj.data);    } 
      else {                          
        presentationErrorCallback(obj.errmsg);    }   
    }  
  } 
});