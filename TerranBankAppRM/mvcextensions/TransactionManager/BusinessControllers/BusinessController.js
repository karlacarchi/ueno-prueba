//Update the base business controller
define([], function () { 
  
  function TransactionManager() { 
    kony.mvc.Business.Controller.call(this); 
  } 
  inheritsFrom(TransactionManager, kony.mvc.Business.Delegator); 
  TransactionManager.prototype.initializeBusinessController = function() { 
  }; 
  TransactionManager.prototype.execute = function(command) { 
    kony.mvc.Business.Controller.prototype.execute.call(this, command);
  };
  
  TransactionManager.prototype.getTransactions = function(presentationSuccessCallback,presentationErrorCallback){        
    var self = this;        
    var transactionList = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("transaction");
    transactionList.customVerb('getTransactions', {}, getTransactionsCompletionCallback);    	    	
    function  getTransactionsCompletionCallback(status,  data,  error) {            
      var srh = applicationManager.getServiceResponseHandler();            
      var obj = srh.manageResponse(status, data, error);            
      kony.print("Data from getTransactions ... "+JSON.stringify(obj));            
      if (obj["status"] === true) {                
        presentationSuccessCallback(obj["data"]);          
      } else {               
        presentationErrorCallback(obj["errmsg"]);           
      }        
    }   
  };
  return TransactionManager;
});