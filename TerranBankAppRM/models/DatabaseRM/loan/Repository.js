define([], function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;

	//Create the Repository Class
	function loanRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};

	//Setting BaseRepository as Parent to this Repository
	loanRepository.prototype = Object.create(BaseRepository.prototype);
	loanRepository.prototype.constructor = loanRepository;

	//For Operation 'applyLoan' with service id 'terranbank_loan_create8977'
	loanRepository.prototype.applyLoan = function(params, onCompletion){
		return loanRepository.prototype.customVerb('applyLoan', params, onCompletion);
	};

	return loanRepository;
})