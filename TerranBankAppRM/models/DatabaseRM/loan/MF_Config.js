/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
	var mappings = {
		"loanid": "loanid",
		"amount": "amount",
		"terms": "terms",
		"interest": "interest",
		"purpose": "purpose",
		"idnumber": "idnumber",
		"phone": "phone",
		"email": "email",
		"isapproved": "isapproved",
		"isclosed": "isclosed",
		"createdtimestamp": "createdtimestamp",
	};

	Object.freeze(mappings);

	var typings = {
		"loanid": "string",
		"amount": "number",
		"terms": "number",
		"interest": "number",
		"purpose": "string",
		"idnumber": "string",
		"phone": "string",
		"email": "string",
		"isapproved": "number",
		"isclosed": "number",
		"createdtimestamp": "date",
	}

	Object.freeze(typings);

	var primaryKeys = [
					"loanid",
	];

	Object.freeze(primaryKeys);

	var config = {
		mappings: mappings,
		typings: typings,
		primaryKeys: primaryKeys,
		serviceName: "DatabaseRM",
		tableName: "loan"
	};

	Object.freeze(config);

	return config;
})