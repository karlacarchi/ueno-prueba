/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
	var mappings = {
		"accountid": "accountid",
		"accountname": "accountname",
		"balance": "balance",
	};

	Object.freeze(mappings);

	var typings = {
		"accountid": "string",
		"accountname": "string",
		"balance": "string",
	}

	Object.freeze(typings);

	var primaryKeys = [
					"accountid",
	];

	Object.freeze(primaryKeys);

	var config = {
		mappings: mappings,
		typings: typings,
		primaryKeys: primaryKeys,
		serviceName: "DatabaseRM",
		tableName: "account"
	};

	Object.freeze(config);

	return config;
})