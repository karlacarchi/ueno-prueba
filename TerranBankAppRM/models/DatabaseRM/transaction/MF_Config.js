/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
	var mappings = {
		"amount": "amount",
		"createdtimestamp": "createdtimestamp",
		"fromaccountid": "fromaccountid",
		"notes": "notes",
		"tid": "tid",
		"toaccountid": "toaccountid",
	};

	Object.freeze(mappings);

	var typings = {
		"amount": "number",
		"createdtimestamp": "date",
		"fromaccountid": "string",
		"notes": "string",
		"tid": "string",
		"toaccountid": "string",
	}

	Object.freeze(typings);

	var primaryKeys = [
					"tid",
	];

	Object.freeze(primaryKeys);

	var config = {
		mappings: mappings,
		typings: typings,
		primaryKeys: primaryKeys,
		serviceName: "DatabaseRM",
		tableName: "transaction"
	};

	Object.freeze(config);

	return config;
})