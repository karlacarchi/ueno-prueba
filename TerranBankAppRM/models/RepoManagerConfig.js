/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function(){
	var repoMapping = {
		loan  : {
			model : "DatabaseRM/loan/Model",
			config : "DatabaseRM/loan/MF_Config",
			repository : "DatabaseRM/loan/Repository",
		},
		account  : {
			model : "DatabaseRM/account/Model",
			config : "DatabaseRM/account/MF_Config",
			repository : "DatabaseRM/account/Repository",
		},
		transaction  : {
			model : "DatabaseRM/transaction/Model",
			config : "DatabaseRM/transaction/MF_Config",
			repository : "DatabaseRM/transaction/Repository",
		},
	};

	return repoMapping;
})