define({ 
  loadAccountList : function (){
    var navManager = applicationManager.getNavigationManager();
    var resultData = navManager.getCustomInfo("frmChangeName");
    this.view.listSelect.masterData = resultData.masterData;
    this.view.forceLayout();
  },
  
  updateAccountName : function(){
    var accountId = this.view.tbxAccountId.text;
    var accountName = this.view.tbxName.text;
    var AccountModule = applicationManager.getModule("AccountModule");
  	AccountModule.presentationController.updateAccountName(accountId, accountName);
  }
});