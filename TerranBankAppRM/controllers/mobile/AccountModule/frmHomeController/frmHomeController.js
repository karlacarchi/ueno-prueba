define({ 
  showAccountsData : function(){ 
    var navManager = applicationManager.getNavigationManager(); 
    var resultData = navManager.getCustomInfo("frmHome"); 

    var widgetDataMap = {   
      "lblAccountid": "accountidmasked",   
      "lblAccountname": "accountname",   
      "lblBalance": "balance" 
    };  
    this.view.segAccounts.widgetDataMap = widgetDataMap;  	 
    this.view.segAccounts.setData(resultData);  
  },

  getTransactions : function(){    
    var transactionModule = applicationManager.getModule("TransactionModule");    	
    transactionModule.presentationController.getTransactionsAndShow();  
  },
  
  displayTransferForm: function(){
    var transferModule = applicationManager.getModule("TransferModule");
    transferModule.presentationController.getAccountsandLoadTransferForm();
  },
  
  loadUpdateAccountForm : function (){
    var accModule = applicationManager.getModule("AccountModule");
    accModule.presentationController.navigateToUpdateAccForm();
  },
  
   displayLoanForm: function(){
    var loanModule = applicationManager.getModule("LoanModule");
    loanModule.presentationController.navigateToLoanForm();
  }

});